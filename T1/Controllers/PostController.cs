﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using T1.BD;
using T1.Models;

namespace T1.Controllers
{
    public class PostController:Controller
    {
        private readonly ILogger<PostController> _logger;
        private AppBlogContex context;
        public PostController(AppBlogContex context, ILogger<PostController> logger)
        {
            this.context = context;
            _logger = logger;

        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Detalle(int id)
        {
            var posts = context.Posts;

            // Traemos el post con el Id de la peticion
            Post post = posts.FirstOrDefault(item => item.Id == id);
            // Traemos los comentarios relaciones a ese post
            List<Comentario> comentarios = context.Comentarios.Where(o => o.post == id).ToList();

            var detalle = new PostDetalle
            {

                Post = post,
                Comentarios = comentarios
            };

            return View("Detalle", detalle);

        }

    }
    public class PostDetalle
    {
        public Post Post { get; set; }
        public List<Comentario> Comentarios { get; set; }
    }
}
